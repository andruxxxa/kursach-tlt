'use strict';

/**
 * @ngdoc function
 * @name frontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the frontApp
 */
angular.module('frontApp')
  .controller('RegisterCtrl', function ($scope) {
    $scope.user = {
      username: "",
      email: "",
      password: ""
    }
    $scope.login = function(){
      $scope.user.username = $scope.user.email.split('@')[0];
      console.log($scope.user.email, $scope.user.username);
    }
  });
