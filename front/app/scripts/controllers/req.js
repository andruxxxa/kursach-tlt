'use strict';

/**
 * @ngdoc function
 * @name frontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the frontApp
 */
angular.module('frontApp')
  .controller('ReqCtrl', function ($scope, $routeParams, Restangular) {
    $scope.slides = ['http://localhost:1337/image/70b2a758-113c-4311-b50a-f5e318518a73.png', 'http://s004.radikal.ru/i208/1010/11/23507773eda2.jpg'];
    $scope.req;
    Restangular.one("request/"+$routeParams.id).get().then(function(data){
      console.log(data);
      var imgs = [];
      data.images.forEach(function(val){
        imgs.push(val.src);
      })
      data.images = imgs;
      $scope.req = data;
    })
  });
