'use strict';

/**
 * @ngdoc function
 * @name frontApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the frontApp
 */
angular.module('frontApp')
  .controller('ControlCtrl', function ($scope, $rootScope, $routeParams, Restangular) {
    $scope.reqs = [];
    $scope.download = function(){
      Restangular.all('admin/all').getList()  // GET: /users
        .then(function(data) {
          // returns a list of users
          $scope.reqs = [];
          console.log(data);
          $scope.reqs = data;
          $scope.reqs.forEach(function(req){
            req.description = req.description.substring(0, 50);
          })
        })
    }
    $scope.download();
    $scope.access = function(id, index){
      Restangular.all("admin/control").post({id:id, status:true}).then(function(data){
        $scope.reqs[index].status = true;
      })
    }
    $scope.noAccess = function(id, index){
      Restangular.all("admin/control").post({id:id, status:false}).then(function(data){
        $scope.reqs[index].status = false;
      })
    }
    $scope.delete = function(id){
      Restangular.all("admin/delete").post({id:id}).then(function(){
        $scope.download();
      })
    }
  });
