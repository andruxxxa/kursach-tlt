'use strict';

/**
 * @ngdoc function
 * @name frontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the frontApp
 */
angular.module('frontApp')
  .controller('AuthCtrl', function ($scope, Restangular, ProfileService, $location) {
        $scope.user = {
            identifier: "",
            password: ""
        }
        $scope.auth = function(){
            Restangular.all("auth").post($scope.user).then(function(data){
                console.log(data);
              if(data.status == "ok") {
                ProfileService.saveState(data.token);
                Restangular.all("whoiam").post({token:data.token}).then(function(i){
                  ProfileService.setStatus(i.status);
                  $location.path("/");
                })
              }
              else{
                alert("Вы не авторизованны");
                $scope.user.identifier = "";
                $scope.user.password = "";
              }
            })
        }
  });
