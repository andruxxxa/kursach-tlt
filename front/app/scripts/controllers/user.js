'use strict';

/**
 * @ngdoc function
 * @name frontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the frontApp
 */
angular.module('frontApp')
  .controller('UserCtrl', function ($scope, $routeParams, Restangular, ProfileService) {
    $scope.user;
    $scope.load = function() {
      Restangular.all("cabinet").post({token: ProfileService.getState()}).then(function (data) {
        $scope.user = data;
      })
    }
    $scope.delete = function(id){
      Restangular.all("req/delete").post({id:id, token:ProfileService.getState()}).then(function(){
        $scope.load();
      })
    }
    $scope.load();
  });
