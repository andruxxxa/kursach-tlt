'use strict';

/**
 * @ngdoc function
 * @name frontApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the frontApp
 */
angular.module('frontApp')
  .controller('PageCtrl', function ($scope, $rootScope, $routeParams, Restangular) {
    $scope.ceil = Math.ceil;
    $scope.nowCount = parseInt($routeParams.id);
    $rootScope.menu(1);
    $scope.reqs = [];
    $scope.count;
    $scope.size;
    Restangular.all("page/"+$routeParams.id).post().then(function(data){
      $scope.size = data.size;
      data = data.reqs;
      console.log(data);
      data.forEach(function(req){
        if($scope.reqs.length == 20) return;
        if(req.images.length == 0){
          req.images.push({src:'http://s004.radikal.ru/i208/1010/11/23507773eda2.jpg'});
        }
        var d = new Date(req.dateCreate);
        //req.shortDesc = req.description.substr(0, 50);
        if(req.description != null){
            console.log(req.description.length);
            req.shortDesc = req.description.substr(0, 50);
            req.shortDesc = "";
        }
        console.log(req.description);
        req.newDate = d.getDate() +"." + (d.getMonth()+1) + "." + d.getFullYear();
        console.log(req.newDate);
        $scope.reqs.push(req);
      })
      console.log("длина", $scope.reqs.length);
      //$scope.reqs = data;
    })
    Restangular.one("req/count").get().then(function(data){
      $scope.count = data.count;
      console.log(data);
    })
  });
