'use strict';

/**
 * @ngdoc function
 * @name frontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the frontApp
 */
angular.module('frontApp')
  .controller('SetCtrl', function ($scope, Restangular, ProfileService, $location, $routeParams) {

    ProfileService.saveState($routeParams.token);
    Restangular.all("whoiam").post({token:$routeParams.token}).then(function(i){
      ProfileService.setStatus(i.status);
      $location.path("/");
    })

  }
);
