'use strict';

/**
 * @ngdoc function
 * @name frontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the frontApp
 */
angular.module('frontApp')
  .controller('LogoutCtrl', function ($scope, $routeParams, Restangular, ProfileService, constants) {
    ProfileService.logout();
    location.href=constants.client;
  });
