'use strict';

/**
 * @ngdoc overview
 * @name frontApp
 * @description
 * # frontApp
 *
 * Main module of the application.
 */
angular
  .module('frontApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angucomplete',
    'oi.file',
    'restangular',
    'LocalStorageModule',
    'restangular',
    'angular-flexslider'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/page/:id', {
        templateUrl: 'views/page.html',
        controller: 'PageCtrl'
      })
      .when('/price/:min/:max/', {
        templateUrl: 'views/price.html',
        controller: 'PriceCtrl'
      })
      .when('/price/:min/:max/:id', {
        templateUrl: 'views/price.html',
        controller: 'PriceCtrl'
      })
      .when('/category/:title/', {
        templateUrl: 'views/category.html',
        controller: 'CategoryCtrl'
      })
      .when('/category/:title/:id', {
        templateUrl: 'views/category.html',
        controller: 'CategoryCtrl'
      })
      .when('/city/:title/', {
        templateUrl: 'views/category.html',
        controller: 'CityCtrl'
      })
      .when('/city/:title/:id', {
        templateUrl: 'views/category.html',
        controller: 'CityCtrl'
      })
      .when('/search/price/:min/:max/category/:catTitle/city/:cityTitle/', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/price/:min/:max/category/:catTitle/city/:cityTitle/query/:key', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/price/:min/:max/category/:catTitle/city/:cityTitle/query/:key/:id', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/city/:cityTitle/query/:key', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/city/:cityTitle/query/:key/:id', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/price/:min/:max/query/:key', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/price/:min/:max/query/:key/:id', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/price/:min/:max/city/:cityTitle/query/:key', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/price/:min/:max/city/:cityTitle/query/:key/:id', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/price/:min/:max/category/:catTitle/query/:key', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/price/:min/:max/category/:catTitle/query/:key/:id', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/category/:catTitle/city/:cityTitle/query/:key', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/category/:catTitle/city/:cityTitle/query/:key/:id', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/price/:min/:max/category/:catTitle/city/:cityTitle/:id', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/price/:min/:max/category/:catTitle/', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/price/:min/:max/category/:catTitle/:id', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/price/:min/:max/city/:cityTitle/', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/price/:min/:max/city/:cityTitle/:id', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/category/:catTitle/city/:cityTitle/', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/search/category/:catTitle/city/:cityTitle/:id', {
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
      })
      .when('/user', {
        templateUrl: 'views/user.html',
        controller: 'UserCtrl'
      })
      .when('/create', {
        templateUrl: 'views/create.html',
        controller: 'CreateCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/set/:token', {
        templateUrl: 'views/set.html',
        controller: 'SetCtrl'
      })
      .when('/auth', {
        templateUrl: 'views/auth.html',
        controller: 'AuthCtrl'
      })
      .when('/register', {
        templateUrl: 'views/register.html',
        controller: 'RegisterCtrl'
      })
      .when('/req/:id', {
        templateUrl: 'views/req.html',
        controller: 'ReqCtrl'
      })
      .when('/query/:key', {
        templateUrl: 'views/search.html',
        controller: 'QueryCtrl'
      })
      .when('/query/:key/:id', {
        templateUrl: 'views/search.html',
        controller: 'QueryCtrl'
      })
      .when('/logout', {
        templateUrl: 'views/about.html',
        controller: 'LogoutCtrl'
      })
      .when('/control', {
        templateUrl: 'views/control.html',
        controller: 'ControlCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  //.constant('constants', {
  //  serverApi: 'http://localhost:1337/',
  //  client: 'http://localhost:1337/front/app/#/'
  //})
  .constant('constants', {
    serverApi: 'http://kursach-tlt.herokuapp.com/',
    client: 'http://kursach-tlt.herokuapp.com/front/app/#/'
  })
  .config(function (RestangularProvider, constants) {
    RestangularProvider.setBaseUrl(constants.serverApi);
    RestangularProvider.setResponseExtractor(function (response) {
      return response;
    })
  })
  .run(function ($document, $rootScope, constants, ProfileService, $location, keyWord, Restangular) {
    $rootScope.count;
    Restangular.one("req/count").get().then(function(data){
      $rootScope.count = data.count;
      console.log(data);
    })
    $rootScope.href = function (url) {
      location.href = constants.client + url;
    }
    $rootScope.test = function () {
      $("#category_global_value").val("112233");
      console.log($("#category_global_value").val());
    }
    $rootScope.profile = ProfileService;
    $("#city_global_value").val("Тольятти");
    var prop = function (wtf) {
      if (!wtf) wtf = 0;
      if ($("body").width() + wtf > 767) {
        $(".sidebar-nav").width($(".container").width() / 4);
        console.log(1);
      }
      else {
        $(".sidebar-nav").width('100%');
      }
    }
    prop();
    $(window).resize(function () {
      prop(15);
    });
    console.log($document.height(), window.innerHeight, window.pageYOffset);
    //if($document.height() - window.innerHeight - window.pageYOffset <=20){
    //  $(".sidebar-nav").css("padding-bottom", 40);
    //}
    $document.scroll(function () {
      console.log($document.height(), window.innerHeight, window.pageYOffset);
      //if($document.height() - window.innerHeight - window.pageYOffset <=20){
      //  $(".sidebar-nav").css("padding-bottom", 20);
      //}
      //else{
      //  $(".sidebar-nav").css("padding-bottom", 0);
      //}
      if (window.pageYOffset > 20) {
        $("body").css("padding-top", 0);
      }
      else {
        $("body").css("padding-top", 20);
      }
    })
    $rootScope.menu = function (num) {
      var els = $("#sidenav01").children();
      for (var i = 0; i < els.length; i++) {
        if ((i + 1) == num) {
          console.log($(els[i]));
          $(els[i]).addClass("active");
        }
        else {
          $(els[i]).removeClass("active");
        }
      }
    }
    $rootScope.constants = constants;
    //Если с английского на русский, то передаём вторым параметром true.
    $rootScope.globalParams = {
      min: "",
      max: "",
      word: ""
    }
    $rootScope.search = function () {
      var str = false;
      var priceP = false;
      var cityP = false;
      var categoryP = false;
      var city = $("#city_global_value").val();
      var category = $("#category_global_value").val();
      if ($rootScope.globalParams.word != "") {
        str = true;
      }
      $rootScope.globalParams.min = parseInt($rootScope.globalParams.min);
      $rootScope.globalParams.max = parseInt($rootScope.globalParams.max);
      console.log($rootScope.globalParams, city, category);
      if (!isNaN($rootScope.globalParams.min)) {
        if ($rootScope.globalParams.min == "" || isNaN($rootScope.globalParams.min)) $rootScope.globalParams.min = 0;
        if ($rootScope.globalParams.max == "" || isNaN($rootScope.globalParams.max)) $rootScope.globalParams.max = -1;
        console.log("число");
        priceP = true;
      }
      if (city != "") cityP = true;
      if (category != "") categoryP = true;
      if (priceP && cityP && categoryP && !str) {
        console.log(constants.client + "search/price/" + $rootScope.globalParams.min + "/" + $rootScope.globalParams.max + "/category/" + $rootScope.transliterate(category) + "/city/" + $rootScope.transliterate(city));
        location.href = constants.client + "search/price/" + $rootScope.globalParams.min + "/" + $rootScope.globalParams.max + "/category/" + $rootScope.transliterate(category) + "/city/" + $rootScope.transliterate(city);
      }
      if (priceP && cityP && categoryP && str) {
        var href = constants.client + "search/price/" + $rootScope.globalParams.min + "/" + $rootScope.globalParams.max + "/category/" + $rootScope.transliterate(category) + "/city/" + $rootScope.transliterate(city) + "/query/" + $rootScope.transliterate($rootScope.globalParams.word);
        console.log(href);
        location.href = href;
      }
      if (!priceP && !cityP && !categoryP && str) {
        var href = constants.client + "query/" + $rootScope.transliterate($rootScope.globalParams.word);
        console.log(href);
        location.href = href;
      }
      if (priceP && !cityP && categoryP && str) {
        var href = constants.client + "search/price/" + $rootScope.globalParams.min + "/" + $rootScope.globalParams.max + "/category/" + $rootScope.transliterate(category) + "/query/" + $rootScope.transliterate($rootScope.globalParams.word);
        console.log(href);
        location.href = href;
      }
      if (!priceP && !cityP && categoryP && !str) {
        var href = constants.client + "category/" + $rootScope.transliterate(category);
        console.log(href);
        location.href = href;
      }
      if (!priceP && cityP && !categoryP && !str) {
        var href = constants.client + "city/" + $rootScope.transliterate(city);
        console.log(href);
        location.href = href;
      }
      if (!priceP && cityP && categoryP && !str) {
        var href = constants.client + "search/category/" + $rootScope.transliterate(category) + "/city/" + $rootScope.transliterate(city);
        console.log(href);
        location.href = href;
      }
      if (priceP && !cityP && categoryP && !str) {
        var href = constants.client + "search/price/" + $rootScope.globalParams.min + "/" + $rootScope.globalParams.max + "/category/" + $rootScope.transliterate(category);
        console.log(href);
        location.href = href;
      }
      if (priceP && !cityP && !categoryP && !str) {
        var href = constants.client + "price/" + $rootScope.globalParams.min + "/" + $rootScope.globalParams.max;
        console.log(href);
        location.href = href;
      }
      if (priceP && !cityP && !categoryP && str) {
        var href = constants.client + "search/price/" + $rootScope.globalParams.min + "/" + $rootScope.globalParams.max + "/query/" + $rootScope.transliterate($rootScope.globalParams.word);
        console.log(href);
        location.href = href;
      }
      if (!priceP && cityP && categoryP && str) {
        var href = constants.client + "search/category/" + $rootScope.transliterate(category) + "/city/" + $rootScope.transliterate(city) + "/query/" + $rootScope.transliterate($rootScope.globalParams.word);
        console.log(href);
        location.href = href;
      }
      if (!priceP && !cityP && categoryP && str) {
        var href = constants.client + "search/category/" + $rootScope.transliterate(category) + "/query/" + $rootScope.transliterate($rootScope.globalParams.word);
        console.log(href);
        location.href = href;
      }
      if (priceP && cityP && !categoryP && !str) {
        var href = constants.client + "search/price/" + $rootScope.globalParams.min + "/" + $rootScope.globalParams.max + "/city/" + $rootScope.transliterate(city);
        console.log(href);
        location.href = href;
      }
      if (priceP && cityP && !categoryP && str) {
        var href = constants.client + "search/price/" + $rootScope.globalParams.min + "/" + $rootScope.globalParams.max + "/city/" + $rootScope.transliterate(city) + "/query/" + $rootScope.transliterate($rootScope.globalParams.word);
        console.log(href);
        location.href = href;
      }
      if (!priceP && cityP && !categoryP && str) {
        var href = constants.client + "search/city/" + $rootScope.transliterate(city) + "/query/" + $rootScope.transliterate($rootScope.globalParams.word);
        console.log(href);
        location.href = href;
      }
    }
    $rootScope.cutId = function (url) {
      for (var i = url.length; i > 0; i--) {
        if (url[i] == "/") {
          return url.substr(0, i);
        }
      }
      return url;
    }
    $rootScope.transliterate = (function () {
      var
        rus = "щ   ш  ч  ц  ю  я  ё  ж  ъ  ы  э  а б в г д е з и й к л м н о п р с т у ф х ь".split(/ +/g),
        eng = "shh sh ch cz yu ya yo zh `` y' e` a b v g d e z i j k l m n o p r s t u f x `".split(/ +/g)
        ;
      return function (text, engToRus) {
        var x;
        for (x = 0; x < rus.length; x++) {
          text = text.split(engToRus ? eng[x] : rus[x]).join(engToRus ? rus[x] : eng[x]);
          text = text.split(engToRus ? eng[x].toUpperCase() : rus[x].toUpperCase()).join(engToRus ? rus[x].toUpperCase() : eng[x].toUpperCase());
        }
        return text;
      }
    })();
    console.log($rootScope.transliterate("Тольятти"));
    console.log($rootScope.transliterate("Компьютерная техника"));
    //var txt = "Съешь ещё этих мягких французских булок, да выпей же чаю!";
    //alert(transliterate(txt));
    //alert(transliterate(transliterate(txt), true));
  })
  .directive('slider', function ($timeout) {
    return {
      restrict: 'AE',
      replace: true,
      scope: {
        images: '='
      },
      link: function (scope, elem, attrs) {
      },
      templateUrl: 'templates/templateurl.html'
    };
  });
;
