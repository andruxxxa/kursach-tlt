'use strict';

angular.module('frontApp')
  .service('ProfileService', function ProfileService(localStorageService, $location) {
    this.profile = null;
    this.status = null;
    this.logout = function(){
      this.profile = null;
      localStorageService.set('token', null);
      localStorageService.set('status', null);
    }
    this.saveState = function(identity){
      this.profile = identity;
      localStorageService.set('token', identity);
    }
    this.getState = function(){
      return localStorageService.get('token');
    }
    this.init = function(){
      if(localStorageService.get('token') == null){
        this.saveState(null);
      };
      if(localStorageService.get('status') == null){
        this.setStatus(null);
      };
    }
    this.myProfile = function(){
      $location.path("user");
    }
    this.setStatus = function(status){
      this.status = status;
      localStorageService.set('status', status);
    }
    this.getStatus = function(){
      return localStorageService.get('status');
    }
    this.init();
  });
