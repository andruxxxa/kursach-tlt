'use strict';

angular.module('frontApp')
  .service('keyWord', function ProfileService(localStorageService, $location) {
    this.profile = null;
    this.logout = function(){
      this.profile = null;
      localStorageService.set('word', null);
    }
    this.saveState = function(identity){
      this.profile = identity;
      localStorageService.set('word', identity);
    }
    this.getState = function(){
      return localStorageService.get('word');
    }
    this.init = function(){
      if(localStorageService.get('word') == null){
        this.saveState(null);
      };
    }
    this.init();
  });
