/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

    '/': 'ViewController.redirect',
    'get /login': 'AuthController.login',
    'get /logout': 'AuthController.logout',
    'get /register': 'AuthController.register',
    'get /uprocess/:id': 'UprocessController.get',
    'post /city': 'CityController.city',
    'post /auth/local': 'AuthController.callback',
    'post /auth/local/:action': 'AuthController.callback', // регистрация
    'post /images/upload': 'ImagesController.upload',
//    'post /auth': "AuthController.auth", // Авторизация

    'post /create': "AdminController.create",
  'get /image/:name': 'ImagesController.image',

    'post /edit/:id':'AdminController.edit',
  'post /admin/detail':'AdminController.story',
    'post /history': 'UserController.history',

    'post /uprocess/action': 'UprocessController.action',

    'post /play': "UserController.play",

    'post /auth/twitter/callback': 'AuthController.twitter',

    'post /check': 'UserController.check',
    'post /myprocess': 'UprocessController.myprocess',
    'post /create': 'AdminController.create',
  'post /req/create': 'RequestController.create',
  'post /cabinet': 'UserController.cabinet',
  'post /req/delete': 'RequestController.delete',
  'post /page/:num': 'RequestController.page',
  'get /req/count': 'RequestController.count',
  'get /req/:id': 'RequestController.get',
  'get /usr/:id': 'UserController.usr',
    'post /delprocess': 'AdminController.delete',
    'delete /user/:id': 'AdminController.delete',
  'post /levels': 'ProcessController.data',
  'post /auth': 'AuthController.auth',
    'get /auth/:provider': 'AuthController.provider',
    'get /auth/:provider/callback': 'AuthController.callback',
  'get /admin/all': 'AdminController.all',
  'post /admin/control': 'AdminController.control',
  'post /admin/delete': 'AdminController.delete',
  'post /whoiam': 'UserController.whoiam'
//    'get /auth/:provider/:action': 'AuthController.callback'

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  *  If a request to a URL doesn't match any of the custom routes above, it  *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
