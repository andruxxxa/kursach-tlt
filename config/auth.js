
var passport = require('passport')
    , GitHubStrategy = require('passport-github').Strategy
    , FacebookStrategy = require('passport-facebook').Strategy
    , GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
    , TwitterStrategy = require('passport-twitter').Strategy;


var verifyHandler = function(token, tokenSecret, profile, done) {
    process.nextTick(function() {
      sails.log.info("Сюда смотри да");
      sails.log.debug(profile);
        User.findOne({username: profile.username}, function(err, user) {
            if (user != undefined) {
                sails.log.info("Да я нашел его!");
                return done(null, user);
            } else {

                var data = {
                    provider: profile.provider,
                    uid: profile.id,
                    name: profile.displayName
                };

                if (profile.emails && profile.emails[0] && profile.emails[0].value) {
                    data.email = profile.emails[0].value;
                }
                if (profile.name && profile.name.givenName) {
                    data.firstname = profile.name.givenName;
                }
                if (profile.name && profile.name.familyName) {
                    data.lastname = profile.name.familyName;
                }
                User.create(data, function(err, user) {
                    return done(err, user);
                });
            }
        });
    });
};

passport.serializeUser(function(user, done) {
  Sails.log.info("Мы тут есть?");
    done(null, user.uid);
});

passport.deserializeUser(function(uid, done) {
  Sails.log.info("Мы тут есть?");
  User.findOne({uid: uid}, function(err, user) {
        done(err, user);
    });
});
//
///**
//* Configure advanced options for the Express server inside of Sails.
//*
//* For more information on configuration, check out:
//* http://sailsjs.org/#documentation
//*/
module.exports.http = {

    customMiddleware: function (app) {

        console.log(__dirname);
        var express = require('../node_modules/sails/node_modules/express');
        app.use('/front', express.static(__dirname + "/../front"));

        passport.use(new GitHubStrategy({
            clientID: "YOUR_CLIENT_ID",
            clientSecret: "YOUR_CLIENT_SECRET",
            callbackURL: "http://localhost:1337/auth/github/callback"
        }, verifyHandler));

        passport.use(new FacebookStrategy({
            clientID: "YOUR_CLIENT_ID",
            clientSecret: "YOUR_CLIENT_SECRET",
            callbackURL: "http://localhost:1337/auth/facebook/callback"
        }, verifyHandler));

        passport.use(new GoogleStrategy({
            clientID: 'YOUR_CLIENT_ID',
            clientSecret: 'YOUR_CLIENT_SECRET',
            callbackURL: 'http://localhost:1337/auth/google/callback'
        }, verifyHandler));

        passport.use(new TwitterStrategy({
            consumerKey: 'D19xzQucrhQog3BaTH60RSVcA',
            consumerSecret: 'JzcCvDtxbkaEhbK8gfmzNiyFyoFvbgtaENwLQ5dxMatwDJLRlU',
            callbackURL: 'http://localhost:1337/auth/twitter/callback'
        }, verifyHandler));

        app.use(passport.initialize());
        app.use(passport.session());
    }
}
module.exports.cache = {

    // The number of seconds to cache files being served from disk
    // (only works in production mode)
    maxAge: 31557600000
};
