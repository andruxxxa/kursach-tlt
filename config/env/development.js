/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {

  /***************************************************************************
   * Set the default database connection for models in the development       *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/
  //domain:"http://bp.mgbeta.ru/",
  //port: 5000,
  words: function (words) {
    if (words.length == 0) return [];
    words = words.replace(/[,.!?*();$#_]/, " ");
    var arr = [];
    arr = words.split(" ");
    //arr.forEach(function(value){
    //  value = value.toLowerCase();
    //})
    for(var i = 0;i<arr.length;i++){
      arr[i] = arr[i].toLowerCase()
    }
    sails.log.debug(arr);
    return arr;
  },
  coin: function(arr1, arr2){
    //sails.log.info(arr1);
    //sails.log.info(arr2);
    var ok = false;
    arr1.forEach(function(value){
      if(_.indexOf(arr2, value)>=0){
        ok = true;
        return;
      }
    })
    return ok;
  },
  part: function (number, arr) {
    if (arr == undefined) return [];
    if (arr.length == 0) return [];
    var ok = [];
    for (var i = 20 * number - 20; i < 20 * number, i < arr.length; i++) {
      ok.push(arr[i]);
    }
    return ok;
  },
  domain: "http://kursach-tlt.herokuapp.com/",
  //domain: "http://localhost:1337/",
  connections: {
    mongodb: {
      adapter: 'sails-mongo',
      host: 'ds063180.mongolab.com',
      port: 63180,
      database: 'kursach',
      user: 'Andruxa',
      password: '112233'
    }
  },

  models: {
    migrate: 'safe',
    connection: 'mongodb'
  },

  admin: {
    username: "Admin",
    password: "Admin",
    email: "admin@gmail.com"
  },
  secret: "zwesxdcrtfvgbyhuAmoABTQ7634luit564LLk"
};
