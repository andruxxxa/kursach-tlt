/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {
    sails.services.passport.loadStrategies();
    //var jwt = require('jwt-simple');
    //var payload = { foo: 'bar' };
    //var secret = 'xxx';
    //var token = jwt.encode(payload, secret);
    //sails.log.info(token);
    //var decoded = jwt.decode(token, secret);
    //sails.log.debug(decoded);
  var bcrypt = require('bcryptjs');
  var salt = bcrypt.genSaltSync(10);
  sails.log.info(bcrypt.compareSync('$2a$10$QvCwze7ZowgqWz4EzOrpPOtmM6kKuqI.jOBcKmisngUrEKB4VX1KG', '$2a$10$QvCwze7ZowgqWz4EzOrpPOtmM6kKuqI.jOBcKmisngUrEKB4VX1KG'));
    Admin.findOne({username:sails.config.admin.username}).exec(function(err, admin){
      if(admin == undefined){
        var bcrypt = require('bcryptjs');
        var salt = bcrypt.genSaltSync(10);
        var newAdmin = {
          username: sails.config.admin.username,
          email: bcrypt.hashSync(sails.config.admin.email, salt),
          password: bcrypt.hashSync(sails.config.admin.password, salt),
          status: "admin"
        }
        Admin.create(newAdmin).exec(function(err){

        })
      }
    })
    Array.prototype.shuffle = function( b )
    {
        var i = this.length, j, t;
        while( i )
        {
            j = Math.floor( ( i-- ) * Math.random() );
            t = b && typeof this[i].shuffle!=='undefined' ? this[i].shuffle() : this[i];
            this[i] = this[j];
            this[j] = t;
        }

        return this;
    };
    // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  cb();
};
