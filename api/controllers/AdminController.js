/**
* AdminController
*
* @description :: Server-side logic for managing admins
* @help        :: See http://links.sailsjs.org/docs/controllers
*/

module.exports = {
    create: function (req, res) {
        var params = req.params.all();
        sails.log.info(params);
        Process.findOne({title: params.title}).exec(function (err, process) {
            if (process != undefined) return res.json({status: 'error', error: 'Имя уже занятно'});
            else {
                params.items.forEach(function(item, index){
                    item.value = index;
                })
                var newProcess = {
                    title: params.title,
                    items: params.items
                }
                Process.create(newProcess).exec(function (err, proc) {
                    if (err) return res.json({status: 'error', error: err});
                    else return res.json({status: 'ok', process: proc});
                })
            }
        })
    },
    delete: function(req, res){
        var params = req.params.all();
        User.destroy({id:params.id}).exec(function(err){
            Passport.destroy({user:params.id}).exec(function(err){
                if(err) return res.send(400);
                return res.send(200);
            })
        })
    },
    edit: function(req, res){
        var params = req.params.all();
        sails.log.info(params);
        return res.send(200);
    },
    create: function(req, res){
        var newProc = {
            title: "Название нового процесса",
            items: [{text:"1", value:0}, {text:"2", value:1}]
        }
        Process.create(newProc).exec(function(err, process){
            return res.json({id:process.id});
        })
    },
    delete: function(req, res){
        var params = req.params.all();
        Process.destroy({id: params.id}).exec(function(err){
            return res.send(200);
        })
    },
    story: function(req, res){
      Uprocess.find({end:true}).exec(function(err, uproc){
        return res.json(uproc);
      })
    },
    control: function(req, res){
      var params = req.params.all();
      Request.findOne({id:params.id}).exec(function(err, data){
        if(data == undefined || err){
          sails.log.error(err);
        }
        else {
          data.status = params.status;
          data.save();
          return res.send(200);
        }
      })
    },
    all: function(req, res){
      Request.find().exec(function(err, reqs){
        return res.json(reqs);
      })
    },
  delete: function(req, res){
    var params = req.params.all();
    Request.destroy({id:params.id}).exec(function(err){
      if(err) return res.send(400);
      else return res.send(200);
    })
  }
};

