module.exports = {
  play: function (req, res) {
      sails.log.info("я тут");
    var params = req.params.all();
    var jwt = require('jwt-simple');
    sails.log.debug(params);
    var user = jwt.decode(params.token, sails.config.secret);
    var u;
    sails.log.info(params);
    Process.findOne({id: params.id}).exec(function (err, process) {
      sails.log.debug(process);
      u = process;
      Uprocess.findOne({processId: process.id, end: false, userId:user.id}).exec(function (err, uproc) {
        sails.log.debug(uproc);
        if (uproc == undefined) {
          if (err)
            return res.json({status: 'error', error: err});
          if (process = undefined)
            return res.json({status: 'error', error: 'Такого process не существует'});
          else {
            sails.log.debug("process", process);
            var uproc = {
              userId: user.id,
              username: user.username,
              title: u.title,
              processId: u.id,
              items: u.items.shuffle(),
              answers: [],
              action: 0,
              history: [],
              timeStart: new Date(),
              timeEnd: new Date(),
              correct: 0,
              incorrect: 0,
              end: false
            }
            Uprocess.create(uproc).exec(function (err, newProc) {
              return res.json(newProc);
            })
          }
        }
        else return res.json(uproc);
      })

    })
  },
  check: function (req, res) {
    var params = req.params.all();
    var jwt = require('jwt-simple');
    sails.log.debug(params);
    var user = jwt.decode(params.token, sails.config.secret);
    Uprocess.findOne({id: params.uprocessId}).exec(function (err, uprocess) {
      if (uprocess == undefined) res.json({
        status: 'error',
        error: 'Такого процесса не найдено'
      })
      else {
        if (uprocess.end == false) {
          uprocess.timeEnd = params.timeEnd;
          uprocess.correct = params.right;
          uprocess.all = uprocess.answers.length + uprocess.items.length - 1;
          uprocess.end = true;
          uprocess.save();
          res.json({
            status: 'ok'
          })
        }
      }
    })
  },
  history: function (req, res) {
    var params = req.params.all();
    var jwt = require('jwt-simple');
    sails.log.debug(params);
    var user = jwt.decode(params.token, sails.config.secret);
    Uprocess.find({userId:user.id}).exec(function (err, uprocess) {
      res.json(uprocess);
    })
  },
  usr: function(req, res){
    var id = req.param('id');
    User.findOne({id:id}).exec(function(err, user){
      if(user == undefined) return res.send(400);
      Request.find({userId:id}).exec(function(err, reqs){
        user.reqs = reqs;
        return res.json(user);
      })
    })
  },
  cabinet: function(req, res){
    var params = req.params.all();
    var jwt = require('jwt-simple');
    sails.log.debug(params);
    var user = jwt.decode(params.token, sails.config.secret);
    User.findOne({id:user.id}).exec(function(err, user){
      if(err || user == undefined){
        sails.log.error(err);
      }
      else{
        Request.find({userId:user.id}).exec(function(err, reqs){
          if(err || reqs == undefined){
            sails.log.error(err);
          }
          else{
            var usr = user;
            user.reqs = reqs;
            return res.json(user);
          }
        })
      }
    })
  },
  whoiam: function(req, res){
    var params = req.params.all();
    var jwt = require('jwt-simple');
    sails.log.debug(params);
    var user = jwt.decode(params.token, sails.config.secret);
    sails.log.error(user);
    if(user.status == "admin") return res.json({status:"admin"});
    if(user.status == undefined || user.status == "user"){
      return res.json({status:"user"});
    }
  }
}
