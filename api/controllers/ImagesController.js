module.exports = {
  upload: function(req, res){
    var skipper = require('skipper');
    var fs = require('fs');
    //
    var uploadFile = req.file('uploadFile');
    console.log(uploadFile);
    uploadFile.upload({ dirname: 'images'},function onUploadComplete (err, files) {
      if(files.length == 0) res.json({
        "status":"error"
      })
      else {// Files will be uploaded to ./assets/images
        if (err) return res.serverError(err);                              // IF ERROR Return and send 500 error with error

        console.log(files);
        return res.json({status: 200, file: files});
      }
    });
  },
  image: function(req, res){
    var fs = require('fs');
    var url = require("url");
    var http = require("http");
    var name = req.param("name");
    var image = fs.readFileSync(".tmp/uploads/images/" + name);
    res.writeHead(200, {'Content-Type': 'image/gif' });
    res.end(image, 'binary');
  }
}
