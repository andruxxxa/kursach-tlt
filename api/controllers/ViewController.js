/**
* AdminController
*
* @description :: Server-side logic for managing admins
* @help        :: See http://links.sailsjs.org/docs/controllers
*/

module.exports = {
    redirect: function(req, res){
        return res.redirect("/front/app/#");
    }
};

