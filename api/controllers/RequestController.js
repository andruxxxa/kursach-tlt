module.exports = {
  create: function(req, res){
    var params = req.params.all();
    var jwt = require('jwt-simple');
    sails.log.debug(params);
    var user = jwt.decode(params.token, sails.config.secret);
    var announce = {
      userId: user.id,
      title: params.title,
      images: params.images,
      dateCreate: new Date(),
      lastView: new Date(),
      description: params.description,
      city: params.city,
      category: params.category,
      contact: params.contact,
      skype: params.skype,
      site: params.site,
      phone: params.phone,
      price: params.price,
      status: false
    }
    Request.create(announce).exec(function(err, announce){
      if(err){
        sails.log.error(err);
        return res.send(400);
      }
      else{
        res.send(200);
        res.json(announce);
      }
    })
  },
  get: function(req, res){
    var id = req.param('id');
    Request.findOne({id:id}).exec(function(err, request){
      request.lastView = new Date();
      request.save();
      return res.json(request);
    })
  },
  count: function(req, res){
    Request.find({status:true}).exec(function(err, data){
      return res.json({count: data.length});
    })
  },
  delete: function(req, res){
    var params = req.params.all();
    var jwt = require('jwt-simple');
    sails.log.debug(params);
    var user = jwt.decode(params.token, sails.config.secret);
    User.findOne({id:user.id}).exec(function(err, usr){
      if(err || usr==undefined){
        sails.log.error(err);
      }
      else{
        Request.findOne({id:params.id, userId:usr.id}).exec(function(err, req){
          if(err || req==undefined){
            sails.log.error(err);
          }
          else{
            Request.destroy({id:req.id}).exec(function(err){
              if(err) return res.send(400);
              else return res.send(200);
            })
          }
        })
      }
    })
  },
  page: function(req, res){
    var num = req.param('num');
    var params = req.params.all();
    var edit = {};
    var active = false;
    if(params.str) {
      sails.log.info("Query есть " + params.str);
      var str = params.str;
      str = sails.config.words(str);
    }
    else str = {length:0};
    if(params.priceStart != undefined && params.priceEnd != undefined) {
      params.priceStart  = parseInt(params.priceStart);
      params.priceEnd = parseInt(params.priceEnd);
      edit.price = {};
      edit.price[">="] = params.priceStart;
      if(params.priceEnd > 0) edit.price["<="] = params.priceEnd;
      active = true;
    }
    if(params.category != undefined){
      edit.category = params.category;
      active = true;
    }
    if(params.city != undefined){
      edit.city = params.city;
      active = true;
    }
    if(active) {
      edit.status = true;
      Request.find(edit).exec(function (err, reqs) {
        if (err) {
          sails.log.error(err);
          return res.send(400);
        }
        else {
          if(str.length > 0) {
            var access = [];
            reqs.forEach(function (req) {
              if (sails.config.coin(sails.config.words(req.title), str)) {
                access.push(req);
              }
            })
            return res.json({reqs: sails.config.part(num, access), size: access.length});
          }
          else return res.json({reqs: sails.config.part(num, reqs), size: reqs.length});
        }
      })
    }
    else{
      Request.find({status:true}).exec(function (err, reqs) {
        if (err) {
          sails.log.error(err);
          return res.send(400);
        }
        else {
          if(str.length > 0) {
            var access = [];
            reqs.forEach(function (req) {
              if (sails.config.coin(sails.config.words(req.title), str)) {
                access.push(req);
              }
            })
            return res.json({reqs: sails.config.part(num, access), size: access.length});
          }
          else return res.json({reqs: sails.config.part(num, reqs), size: reqs.length});
        }
      })
    }
  },
  change: function(req, res){
    var params = req.params.all();
    var jwt = require('jwt-simple');
    sails.log.debug(params);
    var user = jwt.decode(params.token, sails.config.secret);
    var announce = {
      userId: "5489c700e7639a9f1b54724b",
      title: params.title,
      images: params.images,
      dateCreate: new Date(),
      lastView: new Date(),
      description: params.desc,
      city: params.city,
      category: params.category,
      contact: params.contact,
      skype: params.skype,
      site: params.site,
      status: false
    }
    Request.fineOne({userId:user.id, id:id}).exec(function(err, announce){
      if(err || announce == undefined) return res.send(400);
      else{
        announce = {
          title: params.title,
          images: params.images,
          dateCreate: new Date(),
          lastView: new Date(),
          description: params.desc,
          city: params.city,
          category: params.category,
          contact: params.contact,
          skype: params.skype,
          site: params.site
        }
        announce.save();
        res.send(200);
        res.json(announce);
      }
    })
  }
}
