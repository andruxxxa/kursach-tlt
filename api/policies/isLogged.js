module.exports = function(req, res, next){
  var params = req.params.all();
  var jwt = require('jwt-simple');
  sails.log.debug(params);
  var user = jwt.decode(params.token, sails.config.secret);
  User.findOne({id:user.id}).exec(function(err, usr){
    if(usr != undefined && usr.status == "user") next();
    sails.log.info(user);
    sails.log.info("user");
  })

}
