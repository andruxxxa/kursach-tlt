var req = {

  schema: true,

  attributes: {
    userId: {type: 'string'},
    title: {type: 'string'},
    images: {type: 'array'},
    lastView: {type: 'date'},
    dateCreate: {type: 'date'},
    description: {type: 'string'},
    price: {type: 'integer'},
    city: {type: 'string'},
    category: {type: 'string'},
    contact: {type: 'string'},
    skype: {type: 'string'},
    site: {type: 'string'},
    phone: {type: 'string'},
    status: {type: 'boolean'}
  }
};

module.exports = req;
