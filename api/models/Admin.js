
module.exports = {

    attributes: {
        username: 'string',
        email: 'string',
        password: 'string',
        status: 'string'
    }
};

