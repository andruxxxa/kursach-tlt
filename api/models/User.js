var User = {

    schema: true,

    attributes: {
        username  : { type: 'string', unique: true },
        email     : { type: 'email',  unique: true },
        token     : { type: 'string',  unique: true },
        status    : { type: 'string'},
        passports : { collection: 'Passport', via: 'user' }
    }
};

module.exports = User;
